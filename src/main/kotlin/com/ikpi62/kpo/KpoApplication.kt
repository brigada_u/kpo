package com.ikpi62.kpo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KpoApplication

fun main(args: Array<String>) {
	runApplication<KpoApplication>(*args)
}
